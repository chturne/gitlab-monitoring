import os

PROJECTS = {
    'mesa_mirroring_helper': {
        'url': 'https://gitlab.freedesktop.org',
        'project_id': 9348,
        'access_token': os.environ['GOON_GITLAB_ACCESS_TOKEN'],
        'influx_token': os.environ['GOON_INFLUXDB_ACCESS_TOKEN']
    },
    'mesa_valve_ci': {
        'url': 'https://gitlab.freedesktop.org',
        'project_id': 9359,
        'access_token': os.environ['GOON_GITLAB_ACCESS_TOKEN'],
        'influx_token': os.environ['GOON_INFLUXDB_ACCESS_TOKEN']
    },
}
